import React from "react"
import { useEffect,  useState } from "react"

function ConferenceForm() {

    const [name, setName] = useState('')
    const handleNameChange = (e) => {
        const value = e.target.value
        setName(value)
    }

    const [description, setDescription] = useState('')
    const handleDescriptionChange = (e) => {
        const value = e.target.value
        setDescription(value)
    }

    const [start, setStart] = useState('')
    const handleStartChange = (e) => {
      const value = e.target.value
      setStart(value)
    }

    const [end, setEnd] = useState('')
    const handleEndChange = (e) => {
      const value = e.target.value
      setEnd(value)
    }

    const [presentations, setPresentations] = useState('')
    const handlePresentationsChange = (e) => {
        const value = e.target.value
        setPresentations(value)
    }

    const [attendees, setAttendees] = useState('')
    const handleAttendeesChange = (e) => {
        const value = e.target.value
        setAttendees(value)
    }

    const [location, setLocation] = useState('')
    const handleLocationChange = (e) => {
        const value = e.target.value
        setLocation(value)
    }

    const [locations, setLocations] = useState([])
    const fetchData = async () => {

      const locURL = 'http://localhost:8000/api/locations/'
        const loc_response = await fetch(locURL)
        if (!loc_response.ok){
            console.error('Location promise error')
        } else {
            const loc_data = await loc_response.json()
            // console.log(loc_data)
            // console.log("name:" + loc_data.locations)
            setLocations(loc_data.locations)
        }
    }

    useEffect(() => {
      fetchData()
    }, [])

    const handleSubmit = async (e) => {
      e.preventDefault()

      const data = {}
      data.name = name
      data.starts = start
      data.ends = end
      data.description = description
      data.max_presentations = presentations
      data.max_attendees = attendees
      data.location = location
      // console.log(data)


      const confURL = 'http://localhost:8000/api/conferences/'
      const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
          }
      }

      const conf_response = await fetch(confURL, fetchConfig)
      if (!conf_response.ok){
          console.error('Conference promise error')
      } else {
          const newConf = await conf_response.json()
          setName('')
          setStart('')
          setEnd('')
          setDescription('')
          setPresentations('')
          setAttendees('')
          setLocation('')
      }

    }
    return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Create a new conference</h1>
        <form onSubmit={handleSubmit} id="create-conference-form">
          <div className="form-floating mb-3">
            <input value = {name} onChange={handleNameChange} placeholder="Name" required type="text" name ='name' id="name" className="form-control"/>
            <label htmlFor="name">Name</label>
          </div>
          <div className="form-floating mb-3">
            <input value = {start} onChange={handleStartChange} placeholder="starts" required type="datetime-local" name='starts' id="starts" className="form-control"/>
            <label htmlFor="starts">Start date</label>
          </div>
          <div className="form-floating mb-3">
            <input value={end} onChange={handleEndChange} placeholder="ends" required type="datetime-local" name='ends' id="ends" className="form-control"/>
            <label htmlFor="ends">End date</label>
          </div>
          <div className="form-floating mb-3">
            <textarea value={description} onChange={handleDescriptionChange} placeholder="description" required type="text" name="description" id="description" className="form-control"></textarea>
            <label htmlFor="description">Description</label>
          </div>
          <div className="form-floating mb-3">
            <input value={presentations} onChange={handlePresentationsChange} placeholder="max_presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
            <label htmlFor="max_presentations">Max Presentations</label>
          </div>
          <div className="form-floating mb-3">
            <input value={attendees} onChange={handleAttendeesChange} placeholder="max_attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
            <label htmlFor="max_attendees">Max Attendees</label>
          </div>
          <div className="mb-3">
            <select value={location} onChange={handleLocationChange} required id="location" name ='location' className="form-select">
              <option value="">Choose a location</option>
              {locations.map(location => {
                return (<option key={location.id} value={location.id}>{location.name}</option>)
              })}
            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>)
}

export default ConferenceForm
