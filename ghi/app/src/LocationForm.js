import React, { useEffect, useState } from'react';


function LocationForm() {


    const [name, setName] = useState('')
    const handleNameChange = (e) => {
        // console.log(e)
        const value = e.target.value
        setName(value)
        // console.log('name:' + name)
    }

    const [roomcount, setRoom] = useState('')
    const handleRoomChange = (e) => {
        const value = e.target.value
        setRoom(value)
    }


    const [city, setCity] = useState('')
    const handleCityChange = (e) => {
        const value = e.target.value
        setCity(value)
    }

    const [state, setState] = useState('')
    const handleStateChange = (e) => {
        const value = e.target.value
        setState(value)
        console.log(state)
    }

    const [states, setStates] = useState([])
    // console.log(states)
    const fetchData = async () => {
        const stateurl = 'http://localhost:8000/api/states/'
        const state_response = await fetch(stateurl)
        if (!state_response.ok){
            console.error('State promise error')
        } else {
            const state_data = await state_response.json()
            // console.log(state_data)
            setStates(state_data.states)


            // const stateID = document.getElementById('state')
            // for ( let state of state_data.states){
            //     const option = document.createElement('option')
            //     option.value = state.abb
            //     option.innerHTML = state.name
            //     // console.log(option)
            //     stateID.appendChild(option)}
    }
    }

    useEffect(() => {
        fetchData();
      }, []);

      const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.room_count = roomcount;
        data.name = name;
        data.city = city;
        data.state = state;
        console.log(data);


        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          const newLocation = await response.json();
        //   console.log(newLocation);
          setName('');
          setRoom('');
          setCity('');
          setState('');
        }
      }


    return (
<div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name ='name' id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleRoomChange} value={roomcount}placeholder="Room count" required type="number" name='room_count' id="room_count" className="form-control"/>
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCityChange} value={city} placeholder="City" required type="text" name="city" id="city" className="form-control"/>
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
                <select onChange={handleStateChange} value={state} required id="state" name ='state' className="form-select">
                  <option defaultValue="">Choose a state</option>
                  {states.map(state => {
                    return (
                        <option key={state.abb} value={state.abb}>
                            {state.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default LocationForm;
