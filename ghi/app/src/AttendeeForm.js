import React from "react";
import { useEffect, useState } from "react";

function AttendeeForm() {

    const [name, setName] = useState('')
    const handleNameChange = (e) => {
        const value = e.target.value
        setName(value)
    }

    const [email, setEmail] = useState('')
    const handleEmailChange = (e) => {
        const value = e.target.value
        setEmail(value)
    }

    const [conference, setConference] = useState('')
    const handleConferenceChange = (e) => {
        const value = e.target.value
        setConference(value)
        // console.log(value)
    }

    const [conferences, setConferences] = useState([])


    const FetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences)
        // console.log(data.conferences)
        // console.log(conferences)
        const loadTag=document.getElementById('loading-conference-spinner')
        loadTag.classList.add("d-none")
        const ConfTag=document.getElementById('conference')
        ConfTag.classList.remove("d-none")
    }
}

    useEffect(() => {FetchData()}, [])

    const handleSubmit = async (e)=>{
        e.preventDefault()
        const data = {}
        data.email = email
        data.name = name
        data.conference = conference

        // console.log(data)
        const attURL = 'http://localhost:8001/api/attendees/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application.json',
            }
        }
        const att_response = await fetch(attURL, fetchConfig)

        if (att_response.ok){
            const newatt = await att_response.json()
            // console.log(newatt)

            const attendeeform = document.getElementById('create-attendee-form')
            attendeeform.classList.add('d-none')
            const successTag = document.getElementById('success-message')
            successTag.classList.remove('d-none')
        }

    }
    return (
        <div className="my-5">
        <div className="row">
          <div className="col col-sm-auto">
            <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg"/>
          </div>
          <div className="col">
            <div className="card shadow">
              <div className="card-body">
        <form onSubmit={handleSubmit} id="create-attendee-form">
                <h1 className="card-title">It's Conference Time!</h1>
                <p className="mb-3">
                  Please choose which conference
                  you'd like to attend.
                </p>
                <div className="d-flex justify-content-center mb-3" id="loading-conference-spinner">
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                  <select onChange={handleConferenceChange} name="conference" id="conference" className="form-select d-none" required>
                    <option value="">Choose a conference</option>
                    {conferences.map(conference => {
                        return (
                            <option  key={conference.href} value={conference.href}>{conference.name}</option>
                        )
                    })}
                  </select>
                </div>
                <p className="mb-3">
                  Now, tell us about yourself.
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleNameChange} required placeholder="Your full name" type="text" id="name" name="name" className="form-control"/>
                      <label htmlFor="name">Your full name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleEmailChange} required placeholder="Your email address" type="email" id="email" name="email" className="form-control"/>
                      <label htmlFor="email">Your email address</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">I'm going!</button>
              </form>
              <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations! You're all signed up!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    )
}

export default AttendeeForm
