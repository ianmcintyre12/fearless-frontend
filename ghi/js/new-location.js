function errorDiv(error){
    return `<div class="alert alert-dark" role="alert">
    ${error}
  </div>`
}

window.addEventListener('DOMContentLoaded', async () => {

    try {
        const stateurl = 'http://localhost:8000/api/states/'
        const state_response = await fetch(stateurl)
        if (!state_response.ok){
            console.error('State promise error')
        } else {
            const state_data = await state_response.json()
            // console.log(state_data)

            const stateID = document.getElementById('state')
            for ( let state of state_data.states){
                const option = document.createElement('option')
                option.value = state.abb
                option.innerHTML = state.name
                // console.log(option)
                stateID.appendChild(option)
            }

            const formTag = document.getElementById('create-location-form')
            formTag.addEventListener('submit', async event => {
                event.preventDefault()
                // console.log('need to submit form data')
                const formData = new FormData(formTag)
                const json = JSON.stringify(Object.fromEntries(formData))
                console.log(json)

                const locationUrl ='http://localhost:8000/api/locations/'
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application.json',
                    },
                }

                const response = await fetch(locationUrl, fetchConfig)
                if (!response.ok){
                    console.error('location promise error')
                } else {
                    formTag.reset()
                    const newLocation = await response.json()
                    // console.log(newLocation)
                }
            })
     }

    } catch (e) {
        const error = errorDiv(e)
        const errorTag = document.querySelector('.shadow p-4 mt-4')
        errorTag.innerHTML += error
    }
})
