function createCard(name, description, pictureURL,startdate,enddate, location){
    return `
    <div class="card shadow mb-3">
      <img src="${pictureURL}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title mb-2">${name}</h5>
        <h6 class="card-subtitle text-muted mb-2">${location}</h6>
        <p class="card-text">${description}</p>
        <h6 class="card-subtitle">${startdate}-${enddate}</h6>
      </div>
    </div>
  `;
}

function errorDiv(error){
    return `<div class="alert alert-dark" role="alert">
    ${error}
  </div>`
}
window.addEventListener('DOMContentLoaded', async () => {
    try {
        const url ='http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        let i = 0
        const columns = document.querySelectorAll('.col');
        if (!response.ok){
            console.error('Response conference error')
        } else {
            const data = await response.json();

            for (let conference of data.conferences){
            // console.log(conference);

            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok){

                const details = await detailResponse.json();
                console.log(details);

                const startdate_iso = details.conference.starts
                const enddate_iso = details.conference.ends

                const startdateobj =new Date(startdate_iso)//.toDateString()
                const startdate = (startdateobj.getMonth() + 1) + '/' + startdateobj.getDate() + '/' + startdateobj.getFullYear();

                const enddateobj =new Date(enddate_iso)//.toDateString()
                const enddate = (enddateobj.getMonth() + 1) + '/' + enddateobj.getDate() + '/' + enddateobj.getFullYear();

                const conf_details = details.conference.description;
                // console.log(conf_details);


                const html = createCard(conference.name,
                                        conf_details,
                                        details.conference.
                                        location.
                                        picture_url,
                                        startdate,
                                        enddate,
                                        details.conference.location.name);
                // console.log(html);

                const cardCol = i % 3;
                i += 1;
                columns[cardCol].innerHTML += html;

            } else {
                console.error('Conference detail response not ok');
            }
        }
        }
}   catch (e) {

        const error = errorDiv(e)
        const errorTag = document.querySelector('.container')
        errorTag.innerHTML += error
}
});
