window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
      selectTag.classList.remove("d-none")
    //   console.log(selectTag.classList)

      const loadTag = document.getElementById('loading-conference-spinner')
      loadTag.classList.add("d-none")
    //   console.log(loadTag.classList)


      const attendeeform = document.getElementById("create-attendee-form")
      attendeeform.addEventListener('submit', async event =>{


        event.preventDefault()
        const attendeeData = new FormData(attendeeform)
        const json = JSON.stringify(Object.fromEntries(attendeeData))
        // console.log(json)

        const attURL = 'http://localhost:8001/api/attendees/'
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application.json',
            }
        }
        const att_response = await fetch(attURL, fetchConfig)

        if (att_response.ok){
            attendeeform.reset()
            const newatt = await att_response.json()
            // console.log(newatt)
            //// still needs d-none tags
            attendeeform.classList.add('d-none')
            const successTag = document.getElementById('success-message')
            successTag.classList.remove('d-none')

        }
      })
    }

  });
