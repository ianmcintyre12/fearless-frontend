function errorDiv(error){
    return `<div class="alert alert-dark" role="alert">
    ${error}
  </div>`
}


window.addEventListener('DOMContentLoaded', async () => {
    try {
        const locURL = 'http://localhost:8000/api/locations/'
        const loc_response = await fetch(locURL)
        if (!loc_response.ok){
            console.error('Location promise error')
        } else {
            const loc_data = await loc_response.json()
            // console.log(loc_data)
            const locID = document.getElementById('location')
            for (let location of loc_data.locations){
                // console.log(location.id)
                const option = document.createElement('option')
                option.innerHTML = location.name
                option.value = location.id
                locID.appendChild(option)
            }

            const formTag = document.getElementById('create-conference-form')
            formTag.addEventListener('submit', async event =>{
                event.preventDefault()
                const formData = new FormData(formTag)
                const json = JSON.stringify(Object.fromEntries(formData))
                // console.log(json)

                const confURL = 'http://localhost:8000/api/conferences/'
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application.json',
                    }
                }

                const conf_response = await fetch(confURL, fetchConfig)
                if (!conf_response.ok){
                    console.error('Conference promise error')
                } else {
                    formTag.reset()
                    const newConf = await conf_response.json()
                    // console.log(newConf)
                }
            })




        }
    } catch (e) {
        const error = errorDiv(e)
        const errorTag = document.querySelector('.shadow p-4 mt-4')
        errorTag.innerHTML += error
    }
})
